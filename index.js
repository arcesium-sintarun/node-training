import fs from "fs";
import chalk from "chalk";
import axios from "axios";
import express from "express";
import path from "path";

const app = express();
const port = 3000;

const __dirname = path.resolve(); //since we're using es modules syntax

app.use(express.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "/index.html"));
});

/** Fetch posts from the endpoint and save the response locally */
app.get("/fetch-posts", async (req, res) => {
  const API = "https://jsonplaceholder.typicode.com/posts?_limit=5";

  const response = await axios.get(API);
  fs.writeFile("posts.json", JSON.stringify(response.data), (err) => {
    if (err) {
      console.log(chalk.red(err));
      return res.status(500).send("Something went wrong");
    }
  });
  res.send("Posts fetched successfully, and saved to DB!");
});

/** Read the locally saved posts */
app.get("/read-posts", async (req, res) => {
  fs.readFile("posts.json", (err, data) => {
    if (err) {
      console.log(chalk.red(err));
      return res.status(500).send("Something went wrong");
    }
    return res.send(data);
  });
});

/** Delete the locally saved posts */
app.delete("/delete-posts", async (req, res) => {
  fs.unlink("posts.json", (err) => {
    if (err) {
      console.log(chalk.red(err));
      return res.status(500).send("Something went wrong");
    }
    return res.send("Posts deleted successfully");
  });
});

app.listen(port, () => {
  console.log(chalk.bgMagenta(`Server running on port ${port}`));
});
