console.log("hello from javascript");

const fetchPostsButton = document.getElementById("fetch-posts-btn");
const readPostsButton = document.getElementById("read-posts-btn");
const deletePostsButton = document.getElementById("delete-posts-btn");

const contentEl = document.getElementById("content");

fetchPostsButton.addEventListener("click", async () => {
  try {
    const response = await axios.get("/fetch-posts");
    contentEl.innerHTML = "";
    alert(response.data);
  } catch (error) {
    alert("Error fetching posts. Please try again later.", error);
  }
});

readPostsButton.addEventListener("click", async () => {
  try {
    const response = await axios.get("/read-posts");
    contentEl.innerHTML =
      "<pre>" + JSON.stringify(response.data, null, 2) + "</pre>";
  } catch (error) {
    console.error(error);
    contentEl.innerHTML =
      "Error reading posts, no posts found. Please try again later.";
  }
});

deletePostsButton.addEventListener("click", async () => {
  try {
    await axios.delete("/delete-posts");
    contentEl.innerHTML = "";
  } catch (error) {
    console.error(error);
    contentEl.innerHTML = "Error deleting posts. Please try again later.";
  }
});
