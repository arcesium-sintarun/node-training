# node-training

## [Live Website](https://node-training.onrender.com)

### ⛔️ **Important Note** ⛔️

This app is hosted on [Render](https://render.com/) under [free plan](https://render.com/docs/free#free-web-services). This can cause a response delay of up to 30 seconds for the first request that comes in after a period of inactivity.

> Web Services on the free instance type are automatically spun down after 15 minutes of inactivity. When a new request for a free service comes in, Render spins it up again so it can process the request. <br/>
